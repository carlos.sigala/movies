﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSampleMovies.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        private readonly MoviesContext _context;

        public MoviesController(MoviesContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult Index(string search)
        {              
            return View();
        }

        public JsonResult SearchMovies(string search)
        {
            var genres = _context.MovieGenre.Where(x => x.Genre.Name.Contains(search)).Select(x => x.MovieID).ToList().Distinct();
            var actors = _context.MovieActor.Where(x => x.Actor.Name.Contains(search)).Select(x => x.MovieID).ToList().Distinct();
            var movies = _context.Movie
                        .Where(x => x.Name.Contains(search) || genres.Contains(x.Id) || actors.Contains(x.Id))
                        .Select(x => new {
                            x.Id,
                            x.Name,
                            x.Img,
                            x.ReleaseDate
                        }).ToList().Distinct();
            return Json(movies);
        }

        public JsonResult Details(int id)
        {
            var movies = _context.Movie
                       .Where(x => x.Id == id)
                       .Select(x => new {
                           x.Name,
                           x.Overview,
                           Actors = _context.MovieActor.Where(x => x.MovieID == id).Select(x => x.Actor.Name).ToList(),
                           Genre = _context.MovieGenre.Where(x => x.MovieID == id).Select(x => x.Genre.Name).ToList()
                       }).FirstOrDefault();
            return Json(movies);
        }
    }
}
