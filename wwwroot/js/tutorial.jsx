﻿
class CommentBox extends React.Component {
    render() {
        return (
            <div className="container bootdey">
                <Search />
                <span>*Hint: look for Julianne Moore, Hannibal or Spirited Away</span>
                <div className="row" id="divMovies">
                  
                </div>
            </div>
        );
    }
}

function Search() {
    return (
        <div className="row justify-content-md-center">
            <input className="col-4 rounded" id="inputSearch" placeholder="Search by movie title, actor or genre" />
            <button type="button" className="btn btn-primary rounded" id="btnSearch">Search</button>
            
        </div>
    );
}

ReactDOM.render(<CommentBox />, document.getElementById('content'));
