﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSampleMovies.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Img { get; set; }
        [Required]
        public string Overview { get; set; }
        [Required]
        public string Trailer { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public DateTime ReleaseDate { get; set; }

        public virtual MovieGenre MovieGenre { get; set; }
        public virtual MovieActor MovieActor { get; set; }
    }
}
