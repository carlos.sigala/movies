﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSampleMovies.Models
{
    public class MovieActor
    {
        public int Id { get; set; }
        public int MovieID { get; set; }
        public virtual Movie Movie { get; set; }
        public int ActorID { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
