﻿using CodeSampleMovies.Models;
using Microsoft.EntityFrameworkCore;


public class MoviesContext : DbContext
{
    public MoviesContext(DbContextOptions<MoviesContext> options)
           : base(options)
    {
    }
    public DbSet<Actor> Actor { get; set; }
    public DbSet<Genre> Genre { get; set; }
    public DbSet<Movie> Movie { get; set; }
    public DbSet<MovieActor> MovieActor { get; set; }
    public DbSet<MovieGenre> MovieGenre { get; set; }
}
